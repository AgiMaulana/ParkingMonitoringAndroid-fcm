package sendy.kontrakanproject.finalproject.broadcast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by agimaulana on 09/06/17.
 *
 */

public class ParkingLocation implements Serializable{
    @SerializedName("id_location")
    private String idLocation;
    @SerializedName("nama_lokasi")
    private String name;
    @SerializedName("city")
    private String city;
    @SerializedName("alamat")
    private String address;
    private int distance;
    private String latitude;
    private String longitude;
    private int available;
    private List<Camera> cameras;

    public String getIdLocation() {
        return idLocation;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address + ", " + city;
    }

    public int getDistance() {
        return distance;
    }

    public LatLng getPosition(){
        return new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
    }

    public int getAvailable() {
        return available;
    }

    public List<Camera> getCameras() {
        return cameras;
    }
}
