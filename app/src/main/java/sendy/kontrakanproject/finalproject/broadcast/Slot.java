package sendy.kontrakanproject.finalproject.broadcast;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by agimaulana on 09/06/17.
 */

public class Slot implements Serializable{
    @SerializedName("id_slot")
    private int idSLot;
    private int availablity;

    public int getIdSLot() {
        return idSLot;
    }

    public int getAvailablity() {
        return availablity;
    }
}
