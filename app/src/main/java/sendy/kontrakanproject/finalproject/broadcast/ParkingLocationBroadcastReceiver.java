package sendy.kontrakanproject.finalproject.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by agimaulana on 09/06/17.
 */

public class ParkingLocationBroadcastReceiver extends BroadcastReceiver {
    public static final String LOCATION_UPDATE_FILTER = "sendy.kontrakanproject.finalproject.broadcast.LOCATION_UPDATE";
    public static final String LOCATION_DATA = "locations_data";
    private ReceiveCallback receiveCallback;

    public void setReceiveCallback(ReceiveCallback receiveCallback) {
        this.receiveCallback = receiveCallback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String data = bundle.getString(LOCATION_DATA);
            List<ParkingLocation> locations = new Gson().fromJson(data, new TypeToken<List<ParkingLocation>>(){}.getType());
            if (locations != null) {
                if(receiveCallback != null)
                    receiveCallback.onReceive(locations);
            }
        }
    }

    public interface ReceiveCallback{
        void onReceive(List<ParkingLocation> locations);
    }
}
