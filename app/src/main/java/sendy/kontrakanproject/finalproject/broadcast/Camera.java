package sendy.kontrakanproject.finalproject.broadcast;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by agimaulana on 09/06/17.
 */

public class Camera implements Serializable{
    @SerializedName("id_location")
    private String idLocation;
    @SerializedName("camera_ip")
    private String cameraIp;
    @SerializedName("camera_name")
    private String cameraName;
    private int total;
    private int available;
    private List<Slot> slots;

    public String getIdLocation() {
        return idLocation;
    }

    public String getCameraIp() {
        return cameraIp;
    }

    public String getCameraName() {
        return cameraName;
    }

    public int getTotal() {
        return total;
    }

    public int getAvailable() {
        return available;
    }

    public List<Slot> getSlots() {
        return slots;
    }
}
