package sendy.kontrakanproject.finalproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by agimaulana on 14/05/17.
 */

public class SettingUtil {
    private final String PREF_NAME = "setting";
    private final String SETTING_RADIUS = "radius";
    private final String SETTING_FCM_TOKEN = "fcm_token";
    private final String SETTING_LATLNG = "latlng";
    private SharedPreferences sharedPreferences;

    public SettingUtil(Context context){
        this.sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SettingUtil with(Context context){
        return new SettingUtil(context);
    }

    public int getRadius(){
        int radius = sharedPreferences.getInt(SETTING_RADIUS, 10);
        if(radius > 20) {
            setRadius(20);
            return 20;
        }
        return radius;
    }

    public SettingUtil setRadius(int radius){
        sharedPreferences.edit()
                .putInt(SETTING_RADIUS, radius)
                .apply();
        return this;
    }

    public SettingUtil setFcmToken(String token){
        sharedPreferences.edit()
                .putString(SETTING_FCM_TOKEN, token)
                .apply();
        return this;
    }

    public String getFcmToken(){
        return sharedPreferences.getString(SETTING_FCM_TOKEN, "");
    }

    public SettingUtil setLatLng(LatLng latLng){
        sharedPreferences.edit()
                .putString(SETTING_LATLNG, latLng.latitude + "," + latLng.longitude)
                .apply();
        return this;
    }

    public LatLng getLatLng(){
        String str = sharedPreferences.getString(SETTING_LATLNG, "");
        if(TextUtils.isEmpty(str))
            return null;
        String[] strs = str.split(",");
        return new LatLng(Double.valueOf(strs[0]), Double.valueOf(strs[1]));
    }
}
