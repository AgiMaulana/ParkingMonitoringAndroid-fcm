package sendy.kontrakanproject.finalproject;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import sendy.kontrakanproject.finalproject.broadcast.ParkingLocation;
import sendy.kontrakanproject.finalproject.broadcast.ParkingLocationBroadcastReceiver;
import sendy.kontrakanproject.finalproject.service.UpdateLocationService;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMapClickListener,
        SeekBar.OnSeekBarChangeListener, Toolbar.OnMenuItemClickListener, PlaceSelectionListener, LocationBottomSheet.StateListener, ParkingLocationBroadcastReceiver.ReceiveCallback {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private GoogleMap mMap;
    private Circle currCircle;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private BottomSheetBehavior mBottomSheetBehavior;
    private TextView textViewPlace;
    private ArrayList<Marker> markerArrayList;
    private List<ParkingLocation> parkingLocationList;
    private AlertDialog aboutDialog;
    private SeekBar seekBarRadius;
    int end=20;
    private int currentRadius;
    private TextView textViewRadius;
    private LatLng currLatLng;

    private SettingUtil settingUtil;
    private LocationBottomSheet locationBottomSheet;
    private boolean shouldAnimateCamera = true;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 999;

    private View mapView;
    private ParkingLocationBroadcastReceiver parkingLocationBroadcastReceiver;

    private final int DISTANCE_THRESHOLD = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Locale.setDefault(new Locale("in", "ID"));
        setContentView(R.layout.activity_maps);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().hide();
        }
        settingUtil = new SettingUtil(this);
        PlaceAutocompleteFragment autocompleteFragment =
                (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        LatLngBounds bounds = new LatLngBounds(new LatLng(-10.853026, 141.094670),new LatLng(6.018372, 94.629453));
        autocompleteFragment.setBoundsBias(bounds);

        autocompleteFragment.setOnPlaceSelectedListener(this);
        Toolbar toolbarSearch = (Toolbar) findViewById(R.id.toolbar_search_view);
        toolbarSearch.inflateMenu(R.menu.menu_search);
        toolbarSearch.setOnMenuItemClickListener(this);


        textViewPlace = (TextView) findViewById(R.id.place_name);
        textViewRadius = (TextView) findViewById(R.id.text_view_radius);
        seekBarRadius = (SeekBar) findViewById(R.id.seekbar_radius);
        seekBarRadius.setMax(end);
        currentRadius = settingUtil.getRadius() * 100;
        seekBarRadius.setProgress(currentRadius/100);
        seekBarRadius.setVisibility(View.GONE);
        textViewRadius.setVisibility(View.GONE);
        textViewRadius.setText(getString(R.string.radius_, currentRadius));
        seekBarRadius.setOnSeekBarChangeListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        createAboutDialog();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        locationBottomSheet = new LocationBottomSheet(this, R.id.main_content);
        locationBottomSheet.setStateListener(this);
        markerArrayList = new ArrayList<>();
        parkingLocationList = new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        buildGoogleApiClient();
        updateLocationAndRadius();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(parkingLocationBroadcastReceiver == null) {
            parkingLocationBroadcastReceiver = new ParkingLocationBroadcastReceiver();
            parkingLocationBroadcastReceiver.setReceiveCallback(this);
        }
        registerReceiver(parkingLocationBroadcastReceiver,
                new IntentFilter(ParkingLocationBroadcastReceiver.LOCATION_UPDATE_FILTER));

        shouldAnimateCamera = true;
        if(mGoogleApiClient == null || !mGoogleApiClient.isConnected())
            buildGoogleApiClient();
        else
            startFusedLocation();

        locationBottomSheet.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(parkingLocationBroadcastReceiver);
        stopFusedLocation();
        /*if(nearbyLocationTask != null)
            nearbyLocationTask.stop();
        if(locationBottomSheet != null)
            locationBottomSheet.onPause();*/
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Search Parking", "Place: " + place.getName());
                shouldAnimateCamera = false;
                Location location = new Location("PlaceAutocomplete");
                location.setLatitude(place.getLatLng().latitude);
                location.setLongitude(place.getLatLng().longitude);
                onLocationChanged(location);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("Search Parking", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(locationBottomSheet.isShown()){
            locationBottomSheet.hide();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            if(locationBottomSheet.isShown())
                locationBottomSheet.hide();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createAboutDialog(){
        View view = getLayoutInflater().inflate(R.layout.about_us_dialog, null);
        aboutDialog = new AlertDialog.Builder(this)
                .setView(view)
                .create();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if(mGoogleApiClient.isConnected()){
            navigationToCurrLocation();
        }

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.getUiSettings().setCompassEnabled(false);
            }
        } else {
            if(!mGoogleApiClient.isConnected())
                buildGoogleApiClient();
            else
                startFusedLocation();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setOnMapClickListener(this);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ParkingLocation parkingLocation = parkingLocationList.get(markerClickPosition(marker));
                locationBottomSheet.setNearby(parkingLocation);
                locationBottomSheet.show();
                return true;
            }
        });

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 30);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if(mapView != null)
            navigationToCurrLocation();
        startFusedLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void startFusedLocation(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if ((ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && mGoogleApiClient.isConnected())) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void stopFusedLocation(){
        //stop location updates
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    /*public void onMapSearch(View view){
        String location = floatingSearchView.getQuery();
        List<Address> addressList = null;
        if (location.length() > 0) {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title(location));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }*/
    @Override
    public void onLocationChanged(Location location) {
        if(location == null) {
            navigationToCurrLocation();
            return;
        }

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        currLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        if(shouldAnimateCamera){
            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currLatLng, 15));
//            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            shouldAnimateCamera = false;
        }

        /*if(nearbyLocationTask == null) {
            seekBarRadius.setVisibility(View.VISIBLE);
            textViewRadius.setVisibility(View.VISIBLE);
            nearbyLocationTask= new NearbyLocationTask(currLatLng, currentRadius);
            nearbyLocationTask.setOnDataUpdateListener(this);
            nearbyLocationTask.exec();
        }else{
            nearbyLocationTask.setLatLng(currLatLng);
        }*/

        if(settingUtil.getLatLng() != null) {
            LatLng prevLatLng = settingUtil.getLatLng();
            Location locationDest = new Location("Destination");
            locationDest.setLatitude(prevLatLng.latitude);
            locationDest.setLongitude(prevLatLng.longitude);

            float distance = locationDest.distanceTo(locationDest);
            if (distance > DISTANCE_THRESHOLD)
                updateLocationAndRadius();
        }

        seekBarRadius.setVisibility(View.VISIBLE);
        textViewRadius.setVisibility(View.VISIBLE);
        SettingUtil.with(this)
                .setLatLng(currLatLng);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null && mGoogleApiClient.isConnected())
                            startFusedLocation();
                        else
                            buildGoogleApiClient();

                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }
    public void navigationToCurrLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(location!=null){
//                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                shouldAnimateCamera = true;
                onLocationChanged(location);
            }else{
                Toast.makeText(this, "Tidak dapat menentukan lokasi Anda", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        locationBottomSheet.hide();
    }

    private int markerClickPosition(Marker marker){
        for(int i = 0; i < markerArrayList.size(); i++) {
            if (markerArrayList.get(i).getId().equalsIgnoreCase(marker.getId())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        getSharedPreferences("savedRadius", MODE_PRIVATE)
                .edit()
                .putInt("radius", progress)
                .apply();

        currentRadius = progress * 100;
        textViewRadius.setText(getString(R.string.radius_, currentRadius));
        if(currLatLng != null){
            CircleOptions circleOptions = new CircleOptions()
                    .center(currLatLng)
                    .radius(currentRadius)
                    .strokeWidth(2)
                    .strokeColor(Color.BLUE)
                    .fillColor(Color.parseColor("#500084d3"));
            // Supported formats are: #RRGGBB #AARRGGBB
            //   #AA is the alpha, or amount of transparency

            if(currCircle!=null && currCircle.isVisible()){
                currCircle.remove();
            }
            currCircle = mMap.addCircle(circleOptions);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
//        nearbyLocationTask.setRadius(currentRadius);
        settingUtil.setRadius(currentRadius/100);
        updateLocationAndRadius();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
//        if(item.getItemId() == R.id.current_location)
//            navigationToCurrLocation();
        if(item.getItemId() == R.id.about_us)
            aboutDialog.show();
        return true;
    }

    @Override
    public void onPlaceSelected(Place place) {
        // TODO: Get info about the selected place.
        Log.i("Search Location", "Place: " + place.getName());
        shouldAnimateCamera = true;
        Location location = new Location("PlaceAutocomplete");
        location.setLatitude(place.getLatLng().latitude);
        location.setLongitude(place.getLatLng().longitude);
        onLocationChanged(location);
    }

    @Override
    public void onError(Status status) {
        // TODO: Handle the error.
        Log.i("Search Location", "An error occurred: " + status);
    }

    // Location Bottom Sheet StateListener
    @Override
    public void onShow() {
        seekBarRadius.setEnabled(false);
    }

    @Override
    public void onHide() {
        seekBarRadius.setEnabled(true);
    }

    @Override
    public void onReceive(List<ParkingLocation> locations) {
        mMap.clear();
        markerArrayList.clear();
        parkingLocationList.clear();
        parkingLocationList.addAll(locations);
        for(ParkingLocation p : locations){
            markerArrayList.add( mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                    .position(p.getPosition()).title(p.getName())));

            if(locationBottomSheet.isShown()
                    && locationBottomSheet.getParkingLocation().getIdLocation().equalsIgnoreCase(p.getIdLocation()))
                locationBottomSheet.setNearby(p);

            Log.d("Parkir", p.getName());
        }
    }

    private void updateLocationAndRadius(){
        Intent intent = new Intent(this, UpdateLocationService.class);
        startService(intent);
    }
}

