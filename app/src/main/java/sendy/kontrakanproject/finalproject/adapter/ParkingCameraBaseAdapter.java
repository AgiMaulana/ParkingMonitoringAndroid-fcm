package sendy.kontrakanproject.finalproject.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sendy.kontrakanproject.finalproject.R;
import sendy.kontrakanproject.finalproject.broadcast.Camera;

/**
 * Created by agimaulana on 09/06/17.
 */

public class ParkingCameraBaseAdapter extends BaseAdapter {
    private List<Camera> cameras = new ArrayList<>();

    public void swap(List<Camera> cameras){
        this.cameras.clear();
        this.cameras.addAll(cameras);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return cameras.size();
    }

    @Override
    public Camera getItem(int position) {
        return cameras.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_parking_camera, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvName.setText(getItem(position).getCameraName());
        holder.tvTotal.setText(
                parent.getContext()
                        .getString(R.string.total_slot_, getItem(position).getTotal())
        );
        holder.tvAvailable.setText(
                parent.getContext()
                .getString(R.string.available_slot_, getItem(position).getAvailable())
        );

        return convertView;
    }

    static class ViewHolder{
        TextView tvName;
        TextView tvTotal;
        TextView tvAvailable;

        ViewHolder(View itemView){
            tvName = (TextView) itemView.findViewById(R.id.textview_location_name);
            tvTotal = (TextView) itemView.findViewById(R.id.textview_total_slot);
            tvAvailable = (TextView) itemView.findViewById(R.id.textview_available_slot);
        }
    }
}
