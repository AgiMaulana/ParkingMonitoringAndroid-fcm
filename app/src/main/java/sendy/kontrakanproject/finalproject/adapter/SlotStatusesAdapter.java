package sendy.kontrakanproject.finalproject.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sendy.kontrakanproject.finalproject.R;
import sendy.kontrakanproject.finalproject.broadcast.Slot;

/**
 * Created by agimaulana on 16/05/17.
 *
 */

public class SlotStatusesAdapter extends RecyclerView.Adapter<SlotStatusesAdapter.SlotViewHolder>{
    private List<Slot> slotList = new ArrayList<>();

    public void swap(List<Slot> slots){
        this.slotList.clear();
        this.slotList.addAll(slots);
        notifyDataSetChanged();
    }

    @Override
    public SlotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_slot_parking, parent, false);
        return new SlotViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SlotViewHolder holder, int position) {
        Slot slot = slotList.get(position);
        holder.tvNumber.setText(String.valueOf(position+1));
        int srcStatus = slot.getAvailablity() == 0
                ? R.drawable.ic_done_green_500_24dp : R.drawable.ic_clear_red_500_24dp;
        holder.imgStatus.setImageResource(srcStatus);
    }

    @Override
    public int getItemCount() {
        return slotList.size();
    }

    static class SlotViewHolder extends RecyclerView.ViewHolder{
        TextView tvNumber;
        ImageView imgStatus;
        SlotViewHolder(View itemView) {
            super(itemView);
            tvNumber = (TextView) itemView.findViewById(R.id.textview_number);
            imgStatus = (ImageView) itemView.findViewById(R.id.imageview_status);
        }
    }
}
