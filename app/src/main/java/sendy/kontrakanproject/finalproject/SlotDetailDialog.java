package sendy.kontrakanproject.finalproject;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import sendy.kontrakanproject.finalproject.adapter.SlotStatusesAdapter;
import sendy.kontrakanproject.finalproject.broadcast.Camera;

/**
 * Created by agimaulana on 09/06/17.
 *
 */

public class SlotDetailDialog extends AppCompatDialog implements Toolbar.OnMenuItemClickListener {

    private String location;
    private Camera camera;
    private Toolbar toolbar;
    private TextView tvAvailableSlot;
    private TextView tvTotalSlot;
    private RecyclerView recyclerView;
    private SlotStatusesAdapter slotStatusesAdapter;

    public SlotDetailDialog(Context context) {
        super(context, R.style.AppTheme_NoActionBar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_slot_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar != null) {
            toolbar.inflateMenu(R.menu.close);
            toolbar.setOnMenuItemClickListener(this);
        }
        tvTotalSlot = (TextView) findViewById(R.id.textview_total_slot);
        tvAvailableSlot = (TextView) findViewById(R.id.textview_available_slot);
        slotStatusesAdapter = new SlotStatusesAdapter();
        recyclerView = (RecyclerView) findViewById(R.id.recycleViewSlot);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(slotStatusesAdapter);

        if(camera != null) {
            toolbar.setTitle(location);
            toolbar.setSubtitle(camera.getCameraName());
            tvTotalSlot.setText(getContext().getString(R.string.total_slot_, camera.getTotal()));
            tvAvailableSlot.setText(getContext().getString(R.string.available_slot_, camera.getAvailable()));
            slotStatusesAdapter.swap(camera.getSlots());
        }
    }

    public void swapCamera(Camera camera){
        this.camera = camera;
        toolbar.setTitle(location);
        toolbar.setSubtitle(camera.getCameraName());
        if(slotStatusesAdapter != null)
            slotStatusesAdapter.swap(camera.getSlots());
        if(tvTotalSlot != null)
           tvTotalSlot.setText(getContext().getString(R.string.total_slot_, camera.getTotal()));
        if(tvAvailableSlot != null)
            tvAvailableSlot.setText(getContext().getString(R.string.available_slot_, camera.getAvailable()));
    }

    public void show(String location, Camera camera){
        this.location = location;
        this.camera = camera;
        super.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(item.getItemId() == R.id.action_close){
            cancel();
            return true;
        }
        return false;
    }
}
