package sendy.kontrakanproject.finalproject;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sendy.kontrakanproject.finalproject.adapter.ParkingCameraBaseAdapter;
import sendy.kontrakanproject.finalproject.api.ParkingAdapter;
import sendy.kontrakanproject.finalproject.api.ParkingClient;
import sendy.kontrakanproject.finalproject.api.response.ApiResponse;
import sendy.kontrakanproject.finalproject.api.response.data.SlotParkingResponse;
import sendy.kontrakanproject.finalproject.broadcast.Camera;
import sendy.kontrakanproject.finalproject.broadcast.ParkingLocation;

/**
 * Created by agimaulana on 15/05/17.
 *
 */

public class LocationBottomSheet extends BottomSheetBehavior.BottomSheetCallback
    implements LocationListener, Callback<ApiResponse<List<SlotParkingResponse>>>, AdapterView.OnItemClickListener {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private AppCompatActivity activity;
    private BottomSheetBehavior mBottomSheetBehavior;
    private View view;
    private TextView textViewNamePlace;
    private TextView textViewAvailable;
    private TextView textViewDistance;
    private ParkingCameraBaseAdapter cameraBaseAdapter;
    private ListView listView;
    private SlotDetailDialog slotDetailDialog;
    private int selectedCameraPosition = -1;

    private boolean isShown = false;
    private ParkingLocation parkingLocation;
    private LocationManager locationManager;
    private String provider;

    private StateListener stateListener;

    private ParkingClient parkingClient;

    public LocationBottomSheet(AppCompatActivity activity, @IdRes int res) {
        this.activity = activity;
        this.view = activity.findViewById(res);
        this.cameraBaseAdapter = new ParkingCameraBaseAdapter();
        this.slotDetailDialog = new SlotDetailDialog(activity);
        listView = (ListView) view.findViewById(R.id.listview);
        listView.setAdapter(cameraBaseAdapter);
        listView.setOnItemClickListener(this);
        textViewNamePlace = (TextView) view.findViewById(R.id.place_name);
        textViewAvailable = (TextView) view.findViewById(R.id.available_slot);
        textViewDistance = (TextView) view.findViewById(R.id.distance_place);

        mBottomSheetBehavior = BottomSheetBehavior.from(view);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBottomSheetBehavior.setBottomSheetCallback(this);

        parkingClient = ParkingAdapter.create(ParkingClient.class);
    }

    public void onResume(){
        if(isShown)
            calculateDistance();
    }

    public void onPause(){
        stopRequestDistance();
    }

    public void setNearby(ParkingLocation parkingLocation){
        this.parkingLocation = parkingLocation;
        textViewNamePlace.setText(parkingLocation.getName());
        textViewAvailable.setText(String.valueOf(parkingLocation.getAvailable()));
        textViewDistance.setText(activity.getString(R.string.distance, String.valueOf(parkingLocation.getDistance())));
        cameraBaseAdapter.swap(parkingLocation.getCameras());
        if(slotDetailDialog.isShowing() && selectedCameraPosition > -1 && selectedCameraPosition < cameraBaseAdapter.getCount())
            slotDetailDialog.swapCamera(cameraBaseAdapter.getItem(selectedCameraPosition));
//        slotDetailFragmentPagerAdapter = new SlotDetailFragmentPagerAdapter(activity.getSupportFragmentManager(), nearbyLocation.getIdLocation());
//        viewPager.setAdapter(slotDetailFragmentPagerAdapter);
//        viewPager.setAdapter(slotDetailFragmentPagerAdapter);
//        parkingClient.parkingSlot(nearbyLocation.getIdLocation()).enqueue(this);
    }

    public ParkingLocation getParkingLocation() {
        return parkingLocation;
    }

    public void show(){
        isShown = true;
        mBottomSheetBehavior.setPeekHeight(240);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        calculateDistance();

        if(stateListener != null)
            stateListener.onShow();
    }

    public void hide(){
        isShown = false;
        selectedCameraPosition = -1;
//        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    public boolean isShown(){
        return isShown;
    }

    private void showActionBar(){
        if(activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().show();
        }
    }

    private void hideActionBar(){
        if(activity.getSupportActionBar() != null)
            activity.getSupportActionBar().hide();
    }

    public void setStateListener(StateListener stateListener) {
        this.stateListener = stateListener;
    }

    @Override
    public void onStateChanged(@NonNull View bottomSheet, int newState) {
        if(newState == BottomSheetBehavior.STATE_EXPANDED){
            showActionBar();
        }else{
            hideActionBar();
            if(stateListener != null)
                stateListener.onHide();
        }
    }

    @Override
    public void onSlide(@NonNull View bottomSheet, float slideOffset) {

    }

    private void calculateDistance() {
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION
            );
            return;
        }
        locationManager.requestLocationUpdates(provider, 400, 1, this);
        Location location = locationManager.getLastKnownLocation(provider);

        // Initialize the location fields
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
        } /*else {
            Toast.makeText(activity, "Cannot calculate distance.", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void processDistanceCalculating(Location origin) {
        Location locationDest = new Location("Destination");
        locationDest.setLatitude(parkingLocation.getPosition().latitude);
        locationDest.setLongitude(parkingLocation.getPosition().longitude);

        float distance = origin.distanceTo(locationDest);
        textViewDistance.setText(activity.getString(R.string.distance, String.valueOf(round(distance, 1)).substring(0, 3)));
    }

    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    private void stopRequestDistance(){
        if(locationManager != null)
            locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        processDistanceCalculating(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResponse(Call<ApiResponse<List<SlotParkingResponse>>> call, Response<ApiResponse<List<SlotParkingResponse>>> response) {

    }

    @Override
    public void onFailure(Call<ApiResponse<List<SlotParkingResponse>>> call, Throwable t) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Camera camera = cameraBaseAdapter.getItem(position);
        slotDetailDialog.show(parkingLocation.getName(), camera);
        this.selectedCameraPosition = position;
    }

    public interface StateListener{
        public void onShow();
        public void onHide();
    }
}
