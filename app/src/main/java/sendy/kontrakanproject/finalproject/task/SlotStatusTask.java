package sendy.kontrakanproject.finalproject.task;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sendy.kontrakanproject.finalproject.api.ParkingAdapter;
import sendy.kontrakanproject.finalproject.api.ParkingClient;
import sendy.kontrakanproject.finalproject.api.response.ApiResponse;
import sendy.kontrakanproject.finalproject.api.response.data.NearbyLocation;
import sendy.kontrakanproject.finalproject.api.response.data.NearbyLocationResponse;
import sendy.kontrakanproject.finalproject.api.response.data.SlotParking;
import sendy.kontrakanproject.finalproject.api.response.data.SlotParkingResponse;

/**
 * Created by agimaulana on 16/05/17.
 */

public class SlotStatusTask implements Callback<ApiResponse<List<SlotParkingResponse>>> {
    private String parkingId;
    private String cameraName;
    private WaitingTask waitingTask;
    private OnDataUpdateListener onDataUpdateListener;

    public SlotStatusTask(String parkingId, String cameraName) {
        this.parkingId = parkingId;
        this.cameraName = cameraName;
    }

    public void setOnDataUpdateListener(OnDataUpdateListener onDataUpdateListener) {
        this.onDataUpdateListener = onDataUpdateListener;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public void exec(){
        request();
    }

    public void stop(){
        if(waitingTask != null)
            waitingTask.cancel(true);
    }

    private void request(){
        ParkingClient parkingClient = ParkingAdapter.create(ParkingClient.class);
        parkingClient.slotStatuses(parkingId, cameraName).enqueue(this);
    }

    @Override
    public void onResponse(Call<ApiResponse<List<SlotParkingResponse>>> call, Response<ApiResponse<List<SlotParkingResponse>>> response) {
        if(response.isSuccessful() && onDataUpdateListener != null)
            onDataUpdateListener.onDataUpdated(response.body().getData().get(0).getSlots());
        waitingTask = new WaitingTask();
        waitingTask.execute();
    }

    @Override
    public void onFailure(Call<ApiResponse<List<SlotParkingResponse>>> call, Throwable t) {
        waitingTask = new WaitingTask();
        waitingTask.execute();
    }

    private class WaitingTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            Thread.currentThread();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            request();
        }
    }

    public interface OnDataUpdateListener{
        public void onDataUpdated(List<SlotParking> slotParkings);
    }
}
