package sendy.kontrakanproject.finalproject.task;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sendy.kontrakanproject.finalproject.api.ParkingAdapter;
import sendy.kontrakanproject.finalproject.api.ParkingClient;
import sendy.kontrakanproject.finalproject.api.response.ApiResponse;
import sendy.kontrakanproject.finalproject.api.response.data.NearbyLocation;
import sendy.kontrakanproject.finalproject.api.response.data.NearbyLocationResponse;

/**
 * Created by 641430 on 17/03/2017.
 */
public class NearbyLocationTask implements Callback<ApiResponse<NearbyLocationResponse>> {
    private LatLng latLng;
    private int radius;
    private WaitingTask waitingTask;
    private OnDataUpdateListener onDataUpdateListener;

    public NearbyLocationTask(LatLng latLng, int radius) {
        this.latLng = latLng;
        this.radius = radius;
    }

    public void setOnDataUpdateListener(OnDataUpdateListener onDataUpdateListener) {
        this.onDataUpdateListener = onDataUpdateListener;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void exec(){
        request();
    }

    public void stop(){
        if(waitingTask != null)
            waitingTask.cancel(true);
    }

    private void request(){
        ParkingClient parkingClient = ParkingAdapter.create(ParkingClient.class);
        parkingClient.nearbLocation(latLng.latitude, latLng.longitude, radius).enqueue(this);
    }

    @Override
    public void onResponse(Call<ApiResponse<NearbyLocationResponse>> call, Response<ApiResponse<NearbyLocationResponse>> response) {
        if(response.isSuccessful() && onDataUpdateListener != null)
            onDataUpdateListener.onDataUpdated(response.body().getData().getLocations());
        waitingTask = new WaitingTask();
        waitingTask.execute();
    }

    @Override
    public void onFailure(Call<ApiResponse<NearbyLocationResponse>> call, Throwable t) {
        waitingTask = new WaitingTask();
        waitingTask.execute();
    }

    private class WaitingTask extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {
            Thread.currentThread();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            request();
        }
    }

    public interface OnDataUpdateListener{
        public void onDataUpdated(List<NearbyLocation> parkingSlotModels);
    }
}
