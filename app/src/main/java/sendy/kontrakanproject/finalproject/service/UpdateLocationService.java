package sendy.kontrakanproject.finalproject.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import sendy.kontrakanproject.finalproject.SettingUtil;
import sendy.kontrakanproject.finalproject.api.ParkingAdapter;
import sendy.kontrakanproject.finalproject.api.ParkingClient;

/**
 * Created by agimaulana on 09/06/17.
 */

public class UpdateLocationService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public UpdateLocationService() {
        super("UpdateLocationService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SettingUtil settingUtil = new SettingUtil(this);
        LatLng latLng = settingUtil.getLatLng();
        ParkingClient parkingClient = ParkingAdapter.create(ParkingClient.class);
        if(!TextUtils.isEmpty(settingUtil.getFcmToken())) {
            try {
                parkingClient.updateLocation(
                        settingUtil.getFcmToken(),
                        latLng.latitude,
                        latLng.longitude,
                        settingUtil.getRadius() * 100
                ).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
