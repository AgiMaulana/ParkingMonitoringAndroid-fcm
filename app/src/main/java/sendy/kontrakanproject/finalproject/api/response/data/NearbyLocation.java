package sendy.kontrakanproject.finalproject.api.response.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sendy on 5/12/2017.
 */

public class NearbyLocation implements Serializable {
    @SerializedName("id_lokasi")
    private String idLocation;
    @SerializedName("nama_lokasi")
    private String locationName;
    @SerializedName("kota")
    private String city;
    @SerializedName("latitude")
    private double lat;
    @SerializedName("longitude")
    private double lng;
    @SerializedName("alamat")
    private String address;
    private int distance;
    private Slot slot;

    public String getIdLocation() {
        return idLocation;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getCity() {
        return city;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public int getDistance() {
        return distance;
    }

    public Slot getSlot() {
        return slot;
    }

    public class Slot implements Serializable{
        private int total;
        private int available;

        public int getTotal() {
            return total;
        }

        public int getAvailable() {
            return available;
        }
    }


}
