package sendy.kontrakanproject.finalproject.api.response.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sendy on 5/13/2017.
 */

public class SlotParkingResponse implements Serializable{
    @SerializedName("camera_name")
    private String cameraName;
    private int available;
    private int filled;
    @SerializedName("total_slot")
    private int slotTotal;
    private List<SlotParking> slots;
    public String getCameraName() {
        return cameraName;
    }

    public int getAvailable() {

        return available;
    }

    public int getFilled() {
        return filled;
    }

    public int getSlotTotal() {
        return slotTotal;
    }

    public List<SlotParking> getSlots() {
        return slots;
    }
}
