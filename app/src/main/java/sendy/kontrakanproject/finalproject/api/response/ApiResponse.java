package sendy.kontrakanproject.finalproject.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sendy on 5/12/2017.
 */

public class ApiResponse <T>{
    @SerializedName("status")
    private boolean success;
    private String message;
    private T data;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
