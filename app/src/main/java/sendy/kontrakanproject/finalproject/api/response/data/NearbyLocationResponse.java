package sendy.kontrakanproject.finalproject.api.response.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sendy on 5/12/2017.
 */

public class NearbyLocationResponse implements Serializable{
    @SerializedName("origin_latlng")
    private String originLatLng;
    @SerializedName("origin_city")
    private String originCity;
    private int total;
    private List<NearbyLocation> locations;

    public String getOriginLatLng() {
        return originLatLng;
    }

    public String getOriginCity() {
        return originCity;
    }

    public int getTotal() {
        return total;
    }

    public List<NearbyLocation> getLocations() {
        return locations;
    }
}
