package sendy.kontrakanproject.finalproject.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import sendy.kontrakanproject.finalproject.api.response.ApiResponse;
import sendy.kontrakanproject.finalproject.api.response.data.NearbyLocationResponse;
import sendy.kontrakanproject.finalproject.api.response.data.SlotParkingResponse;

/**
 * Created by sendy on 21-Feb-17.
 *
 */

public interface ParkingClient {
    @GET("/parking/nearby-location")
    Call<ApiResponse<NearbyLocationResponse>> nearbLocation(@Query("lat") double lat, @Query("lng") double lng, @Query("radius") int radius);

    @GET("/parking/slot")
    Call<ApiResponse<List<SlotParkingResponse>>> parkingSlot(@Query("parking") String parkingId);

    @GET("/parking/slot/status")
    Call<ApiResponse<List<SlotParkingResponse>>> slotStatuses(@Query("parking") String parkingId,
                                                             @Query("camera") String camera);

    @FormUrlEncoded
    @POST("/user/update/token")
    Call<ApiResponse> updateToken(@Field("old_token") String oldToken, @Field("new_token") String newToken);

    @FormUrlEncoded
    @POST("/user/update/location")
    Call<ApiResponse> updateLocation(@Field("fcm_token") String token,
                                         @Field("lat") double lat,
                                         @Field("lng") double lng,
                                         @Field("radius") int radius);
}
