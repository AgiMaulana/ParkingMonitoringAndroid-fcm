package sendy.kontrakanproject.finalproject.api.response.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sendy on 5/13/2017.
 */

public class SlotParking {
    @SerializedName("id_slot")
    private String idLocation;
    @SerializedName("id_location")
    private String idSlot;
    @SerializedName("datetime")
    private String dateTime;
    private String availablity;
    private int number;

    public String getIdLocation() {
        return idLocation;
    }

    public String getIdSlot() {
        return idSlot;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getAvailablity() {
        return availablity;
    }

    public int getNumber() {
        return number;
    }

}
